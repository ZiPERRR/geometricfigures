import java.util.Objects;

public class Triangle {

    private float firstAngle;
    private float firstSide;
    private float secondSide;
    private float thirdSide;

    public Triangle(int firstS, int secondS, int thirdS) {
        this.firstSide = firstS;
        this.secondSide = secondS;
        this.thirdSide = thirdS;
    }

    public Triangle(float firstS, float secondS, float firstA) {
        this.firstSide = firstS;
        this.secondSide = secondS;
        this.firstAngle = firstA;
        this.thirdSide = (float) Math.sqrt(Math.pow(secondS, 2) + Math.pow(firstS, 2) - 2 * secondS * firstS * Math.cos(firstA));
    }

    public float getPerimeter() {
        return firstSide + secondSide + thirdSide;
    }

    public double getArea() {
        double halfPerimeter = 0.5 * (firstSide + secondSide + thirdSide);
        return Math.sqrt(halfPerimeter * (halfPerimeter - firstSide) * (halfPerimeter - secondSide) * (halfPerimeter - thirdSide));

        //return 0.5 * height * base;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Triangle triangle = (Triangle) o;
        return firstSide == triangle.firstSide &&
                secondSide == triangle.secondSide &&
                thirdSide == triangle.thirdSide || firstSide == triangle.firstSide &&
                secondSide == triangle.secondSide && firstAngle == triangle.firstAngle;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstSide, secondSide, thirdSide);
    }
}